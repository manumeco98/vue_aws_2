import {API, Auth} from "aws-amplify"

const arimeticOperationServices = {}

arimeticOperationServices.postAdd = async function (operand1, operand2) {
  const user = await Auth.currentAuthenticatedUser()
  const token = user.signInUserSession.idToken.jwtToken
  const options = {
    headers:
    {
      Authorization: token
    },
    body: {
      operand1,
      operand2
    }
  }
  try{
  const response = await API.post("vueaws2api10","/aricmetic/add", options)
  return response
  } catch(error){
    console.error(error)
    return {error, status:500}
  }
  
}
arimeticOperationServices.postSubstract = async function (operand1, operand2) {
  const user = await Auth.currentAuthenticatedUser()
  const token = user.signInUserSession.idToken.jwtToken
  const options = {
    headers:
    {
      Authorization: token
    },
    body: {
      operand1,
      operand2
    }
  }
  try{
  const response = await API.post("vueaws2api10","/aricmetic/substract", options)
  return response
  } catch(error){
    console.error(error)
    return {error, status:500}
  }
}
arimeticOperationServices.postMultiply = async function (operand1, operand2) {
  const user = await Auth.currentAuthenticatedUser()
  const token = user.signInUserSession.idToken.jwtToken
  const options = {
    headers:
    {
      Authorization: token
    },
    body: {
      operand1,
      operand2
    }
  }
  try{
  const response = await API.post("vueaws2api10","/aricmetic/multiply", options)
  return response
  } catch(error){
    console.error(error)
    return {error, status:500}
  }
}

arimeticOperationServices.postDivide = async function (operand1, operand2) {
  const user = await Auth.currentAuthenticatedUser()
  const token = user.signInUserSession.idToken.jwtToken
  const options = {
    headers:
    {
      Authorization: token
    },
    body: {
      operand1,
      operand2
    }
  }
  try{
  const response = await API.post("vueaws2api10","/aricmetic/divide", options)
  return response
  } catch(error){
    console.error(error)
    return {error, status:500}
  }
}
export default arimeticOperationServices
